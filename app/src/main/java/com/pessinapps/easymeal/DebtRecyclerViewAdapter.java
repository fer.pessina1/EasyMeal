package com.pessinapps.easymeal;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.pessinapps.easymeal.firebase.MealDataManager;
import com.pessinapps.easymeal.firebase.model.Debt;
import com.pessinapps.easymeal.firebase.model.MealExpenseData;
import com.pessinapps.easymeal.firebase.model.Payment;
import com.pessinapps.easymeal.firebase.model.Person;
import com.pessinapps.easymeal.util.RecyclerViewAdapterCallback;

import java.util.List;

/**
 * Created by fernando.pessina on 18/10/2017.
 *
 * Custom adapter for expenses fragment
 */

public class DebtRecyclerViewAdapter extends RecyclerView.Adapter<DebtRecyclerViewAdapter.DebtViewHolder> {

    private List<Debt> debtsList;
    private Context context;
    private RecyclerViewAdapterCallback adapterCallback;

    private static final int NEW_PAYMENT = 1;

    class DebtViewHolder extends RecyclerView.ViewHolder {
        TextView mainTextView;
        ImageButton makePaymentButton;
        View debtLayout;
        DebtViewHolder(View view) {
            super(view);
            mainTextView = view.findViewById(R.id.tv_debt_list);
            makePaymentButton = view.findViewById(R.id.bt_settle_debt);
            debtLayout = view;
        }
    }

    public DebtRecyclerViewAdapter(Context context, List<Debt> debtsList, RecyclerViewAdapterCallback callback) {
        this.adapterCallback = callback;
        this.debtsList = debtsList;
        this.context = context;
    }

    @Override
    public DebtViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.debt_list_item, parent, false);
        return new DebtViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DebtViewHolder holder, int position) {
        Debt debt = debtsList.get(position);
        final Person creditor = debt.getCreditor();
        final Float amount = debt.getAmount();
        final Person debtor = debt.getDebtor();

        String gen1 = " " + context.getString(R.string.owes) + " ";
        String amountString = " $" + String.format(java.util.Locale.US,"%.2f", amount);
        String to = " " + context.getString(R.string.to) + " ";
        final SpannableStringBuilder str = new SpannableStringBuilder(debtor.getName() + gen1 + amountString + to + creditor.getName());
        str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), debtor.getName().length() + gen1.length(), str.length() - to.length() - creditor.getName().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        str.setSpan(new android.text.style.ForegroundColorSpan(ContextCompat.getColor(context, R.color.colorAccent)), debtor.getName().length() + gen1.length(), str.length() - to.length() - creditor.getName().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        str.append(".");
        holder.mainTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_debts_24px, 0, 0, 0);
        holder.mainTextView.setCompoundDrawablePadding((int) (8 * Resources.getSystem().getDisplayMetrics().density));
        holder.mainTextView.setText(str);

        holder.mainTextView.setPadding((int) (16 * Resources.getSystem().getDisplayMetrics().density)
                , (int) (8 * Resources.getSystem().getDisplayMetrics().density)
                , (int) (16 * Resources.getSystem().getDisplayMetrics().density)
                , (int) (8 * Resources.getSystem().getDisplayMetrics().density));

        MealExpenseData mealData = MealDataManager.getInstance().getData();
        holder.makePaymentButton.setTag(R.string.from_tag, mealData.getMembers().indexOf(debtor));
        holder.makePaymentButton.setTag(R.string.to_tag, mealData.getMembers().indexOf(creditor));
        holder.makePaymentButton.setTag(R.string.amount_tag, amount);
        holder.makePaymentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Open new payment activity with loaded data
                Payment newPayment = new Payment(debtor, creditor, amount);
                adapterCallback.onMethodCallback(newPayment, NEW_PAYMENT);
            }
        });
    }

    @Override
    public int getItemCount() {
        return debtsList.size();
    }
}
