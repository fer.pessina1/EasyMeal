package com.pessinapps.easymeal;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.pessinapps.easymeal.activities.DebtsFragment;
import com.pessinapps.easymeal.activities.ExpenseFragment;
import com.pessinapps.easymeal.activities.MembersFragment;
import com.pessinapps.easymeal.activities.OverviewFragment;

public class ExpenseAdapter extends FragmentStatePagerAdapter {

    private int numberOfTabs;
    private String[] tabTitles;
    private SparseArray<Fragment> registeredFragments = new SparseArray<>();

    public ExpenseAdapter(FragmentManager fm, String[] tabTitles) {
        super(fm);
        this.tabTitles = tabTitles;
        this.numberOfTabs = tabTitles.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new OverviewFragment();
            case 1:
                return new ExpenseFragment();
            case 2:
                return new DebtsFragment();
            case 3:
                return new MembersFragment();
            default:
                return null;
        }
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
        registeredFragments.remove(position);
    }

    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }

    @Override
    public int getCount() {
        return numberOfTabs;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}
