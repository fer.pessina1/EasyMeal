package com.pessinapps.easymeal;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.pessinapps.easymeal.firebase.MealDataManager;
import com.pessinapps.easymeal.firebase.model.Expense;
import com.pessinapps.easymeal.firebase.model.Person;
import com.pessinapps.easymeal.util.RecyclerViewAdapterCallback;

import java.util.List;

/**
 * Created by fernando.pessina on 18/10/2017.
 *
 * Custom adapter for expenses fragment
 */

public class ExpenseRecyclerViewAdapter extends RecyclerView.Adapter<ExpenseRecyclerViewAdapter.ExpenseViewHolder> {

    private List<Expense> expensesList;
    private Context context;
    private RecyclerViewAdapterCallback adapterCallback;
    private Person activeMember;

    private static final int EDIT_EXPENSE = 2;
    private static final int DELETE_EXPENSE = 3;

    public class ExpenseViewHolder extends RecyclerView.ViewHolder {
        public TextView main, detail;
        public ImageButton menuButton, editButton, deleteButton;
        public View expenseLayout;
        public ExpenseViewHolder(View view) {
            super(view);
            main = view.findViewById(R.id.tv_expense_main);
            detail = view.findViewById(R.id.tv_expense_detail);
            menuButton = view.findViewById(R.id.ib_options);
            editButton = view.findViewById(R.id.ib_edit);
            deleteButton = view.findViewById(R.id.ib_delete);
            expenseLayout = view;
        }
    }

    public ExpenseRecyclerViewAdapter(Context context, List<Expense> expensesList, RecyclerViewAdapterCallback callback, Person activeMember) {
        this.adapterCallback = callback;
        this.expensesList = expensesList;
        this.context = context;
        this.activeMember = activeMember;
    }

    @Override
    public ExpenseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.expense_list_item, parent, false);
        return new ExpenseViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ExpenseViewHolder holder, int position) {
        Expense expense = expensesList.get(position);
        if (expense.getCreator().equals(activeMember)) {
            // Load view with data
            String aux1 = " " + context.getString(R.string.paid) + " ";
            String amount = "$" + expense.getAmount();
            String aux2 = " " + context.getString(R.string.for1) + " ";
            final SpannableStringBuilder str = new SpannableStringBuilder(expense.getCreator().getName() + aux1 + amount + aux2 + expense.getConcept());
            holder.main.setText(str);

            if (expense.getBeneficiaries().containsAll(MealDataManager.getInstance().getData().getMembers())) {
                String aux3 = context.getString(R.string.people_involved) + " ";
                String people = context.getString(R.string.everyone);
                final SpannableStringBuilder str2 = new SpannableStringBuilder(aux3 + people);
                holder.detail.setText(str2);
            } else {
                String aux3 = context.getString(R.string.people_involved) + " ";
                String people = "";
                for (int i = 0; i < expense.getBeneficiaries().size(); i++) {
                    if (i < expense.getBeneficiaries().size() - 1) {
                        people += expense.getBeneficiaries().get(i).getName() + ", ";
                    } else {
                        people += expense.getBeneficiaries().get(i).getName();
                    }
                }
                final SpannableStringBuilder str2 = new SpannableStringBuilder(aux3 + people);
                holder.detail.setText(str2);
            }
        } else {
            String aux1 = " " + context.getString(R.string.paid) + " ";
            String amount = "$" + expense.getAmount();
            String aux2 = " " + context.getString(R.string.for1) + " ";
            final SpannableStringBuilder str = new SpannableStringBuilder(expense.getCreator().getName() + aux1 + amount + aux2 + expense.getConcept());
            holder.main.setText(str);
            if (expense.getBeneficiaries().containsAll(MealDataManager.getInstance().getData().getMembers())) {
                String aux3 = context.getString(R.string.people_involved) + " ";
                String people = context.getString(R.string.everyone);
                final SpannableStringBuilder str2 = new SpannableStringBuilder(aux3 + people);
                holder.detail.setText(str2);
            } else {
                String aux3 = context.getString(R.string.people_involved) + " ";
                String people = "";
                for (int i = 0; i < expense.getBeneficiaries().size(); i++) {
                    if (i < expense.getBeneficiaries().size() - 1) {
                        people += expense.getBeneficiaries().get(i).getName() + ", ";
                    } else {
                        people += expense.getBeneficiaries().get(i).getName();
                    }
                }
                final SpannableStringBuilder str2 = new SpannableStringBuilder(aux3 + people);
                holder.detail.setText(str2);
            }
        }
        holder.editButton.setTag(R.id.expense, expense);
        holder.editButton.setTag(R.id.menu_button, holder.menuButton);
        holder.deleteButton.setTag(R.id.expense, expense);
        holder.deleteButton.setTag(R.id.menu_button, holder.menuButton);
        holder.editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageButton menuButton = (ImageButton) view.getTag(R.id.menu_button);
                menuButton.callOnClick();
                Expense editExpense = (Expense) view.getTag(R.id.expense);
                adapterCallback.onMethodCallback(editExpense, EDIT_EXPENSE);
            }
        });
        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageButton menuButton = (ImageButton) view.getTag(R.id.menu_button);
                menuButton.callOnClick();
                final Expense deleteExpense = (Expense) view.getTag(R.id.expense);
                adapterCallback.onMethodCallback(deleteExpense, DELETE_EXPENSE);
            }
        });
        holder.menuButton.setTag(R.layout.expense_list_item, holder.expenseLayout);
        holder.menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View expenseLayout = (View) view.getTag(R.layout.expense_list_item);
                ImageButton edit = expenseLayout.findViewById(R.id.ib_edit);
                ImageButton delete = expenseLayout.findViewById(R.id.ib_delete);
                TextView text1 = expenseLayout.findViewById(R.id.tv_expense_main);
                TextView text2 = expenseLayout.findViewById(R.id.tv_expense_detail);
                ImageButton button = expenseLayout.findViewById(R.id.ib_options);
                if (edit.getVisibility() == View.GONE) {
                    edit.setVisibility(View.VISIBLE);
                    delete.setVisibility(View.VISIBLE);
                    button.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_cancel2_24px));
                    text1.setAlpha(0.4F);
                    text2.setAlpha(0.4F);
                } else {
                    edit.setVisibility(View.GONE);
                    delete.setVisibility(View.GONE);
                    button.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_3dot_24px));
                    text1.setAlpha(1F);
                    text2.setAlpha(1F);
                }
            }
        });
        if(holder.editButton.getVisibility() == View.VISIBLE){
            holder.menuButton.callOnClick();
        }
    }

    @Override
    public int getItemCount() {
        return expensesList.size();
    }
}
