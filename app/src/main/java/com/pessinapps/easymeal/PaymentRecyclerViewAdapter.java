package com.pessinapps.easymeal;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.pessinapps.easymeal.firebase.model.Payment;
import com.pessinapps.easymeal.util.RecyclerViewAdapterCallback;

import java.util.List;

/**
 * Created by fernando.pessina on 24/10/2017.
 */

public class PaymentRecyclerViewAdapter extends RecyclerView.Adapter<PaymentRecyclerViewAdapter.PaymentViewHolder> {

    private List<Payment> paymentsList;
    private Context context;
    private RecyclerViewAdapterCallback adapterCallback;

    private static final int DELETE_PAYMENT = 2;

    public PaymentRecyclerViewAdapter(Context context, List<Payment> paymentsList, RecyclerViewAdapterCallback callback) {
        this.adapterCallback = callback;
        this.paymentsList = paymentsList;
        this.context = context;
    }

    class PaymentViewHolder extends RecyclerView.ViewHolder {
        TextView mainTextView;
        ImageButton deletePaymentButton;
        View paymentLayout;
        PaymentViewHolder(View view) {
            super(view);
            mainTextView = view.findViewById(R.id.tv_payment_list);
            deletePaymentButton = view.findViewById(R.id.bt_delete_payment);
            paymentLayout = view;
        }
    }

    @Override
    public PaymentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.payment_list_item, parent, false);
        return new PaymentViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PaymentViewHolder holder, int position) {
        Payment payment= paymentsList.get(position);
        Float amount = payment.getAmount();
        String aux1 = " " + context.getString(R.string.paid) + " ";
        String amountString = " $" + String.format(java.util.Locale.US,"%.2f", amount);
        String to = " " + context.getString(R.string.to) + " ";
        final SpannableStringBuilder str = new SpannableStringBuilder(payment.getFrom().getName() + aux1 + amountString + to + payment.getTo().getName());
        str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), payment.getFrom().getName().length() + aux1.length(), str.length() - to.length() - payment.getTo().getName().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        str.setSpan(new android.text.style.ForegroundColorSpan(ContextCompat.getColor(context, R.color.colorAccent)), payment.getFrom().getName().length() + aux1.length(), str.length() - to.length() - payment.getTo().getName().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        holder.mainTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_debts_24px, 0, 0, 0);
        holder.mainTextView.setCompoundDrawablePadding((int) (8 * Resources.getSystem().getDisplayMetrics().density));
        holder.mainTextView.setText(str);
        holder.deletePaymentButton.setTag(R.string.payment_tag, payment);
        holder.deletePaymentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Payment payment1 = (Payment) view.getTag(R.string.payment_tag);
                adapterCallback.onMethodCallback(payment1, DELETE_PAYMENT);
            }
        });
    }

    @Override
    public int getItemCount() {
        return paymentsList.size();
    }

}
