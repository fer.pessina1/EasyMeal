package com.pessinapps.easymeal.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.pessinapps.easymeal.DebtRecyclerViewAdapter;
import com.pessinapps.easymeal.PaymentRecyclerViewAdapter;
import com.pessinapps.easymeal.R;
import com.pessinapps.easymeal.firebase.MealDataManager;
import com.pessinapps.easymeal.firebase.model.Debt;
import com.pessinapps.easymeal.firebase.model.MealExpenseData;
import com.pessinapps.easymeal.firebase.model.Payment;
import com.pessinapps.easymeal.firebase.model.Person;
import com.pessinapps.easymeal.util.BalanceCalculator;
import com.pessinapps.easymeal.util.DataChangeListener;
import com.pessinapps.easymeal.util.RecyclerViewAdapterCallback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class DebtsFragment extends Fragment implements RecyclerViewAdapterCallback {

    private MealExpenseData mealExpenseData;
    private Person activeMember;

    private RecyclerView debtsContainer;
    private DebtRecyclerViewAdapter debtsAdapter;
    private List<Debt> displayedDebtsList = new ArrayList<>();
    private TextView noDebtsTextView;

    private RecyclerView paymentsContainer;
    private PaymentRecyclerViewAdapter paymentsAdapter;
    private List<Payment> displayedPaymentsList = new ArrayList<>();
    private TextView noPaymentsTextView;

    private Spinner spinner;
    private Button newPaymentButton;
    private LinearLayout debtsHeader, paymentsHeader;
    private ProgressBar progressBar;

    private Map<View,Payment> paymentsMap = new HashMap<>();
    private Map<View,Debt> debtsMap = new HashMap<>();

    private static final int NEW_PAYMENT = 1;
    private static final int DELETE_PAYMENT = 2;

    private MealDataManager mealDataManager;
    private DataChangeListener listener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.payments_fragment, container, false);

        mealDataManager = MealDataManager.getInstance();
        mealExpenseData = mealDataManager.getData();

        spinner = layout.findViewById(R.id.filterSpinner);
        debtsHeader = layout.findViewById(R.id.ll_debts_header);
        paymentsHeader = layout.findViewById(R.id.ll_payments_header);
        progressBar = layout.findViewById(R.id.paymentsProgressBar);

        debtsContainer = layout.findViewById(R.id.rv_debts);
        debtsAdapter = new DebtRecyclerViewAdapter(getContext(), displayedDebtsList, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        debtsContainer.setLayoutManager(mLayoutManager);
        DefaultItemAnimator animator = new DefaultItemAnimator();
        debtsContainer.setItemAnimator(animator);
        debtsContainer.setAdapter(debtsAdapter);
        noDebtsTextView = layout.findViewById(R.id.tv_no_debts);

        paymentsContainer= layout.findViewById(R.id.rv_payments);
        paymentsAdapter = new PaymentRecyclerViewAdapter(getContext(), displayedPaymentsList, this);
        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getContext());
        paymentsContainer.setLayoutManager(mLayoutManager1);
        DefaultItemAnimator animator1 = new DefaultItemAnimator();
        paymentsContainer.setItemAnimator(animator1);
        paymentsContainer.setAdapter(paymentsAdapter);
        noPaymentsTextView = layout.findViewById(R.id.tv_no_payments);

        final SpannableStringBuilder str = new SpannableStringBuilder(getString(R.string.generic_no_debts));
        str.setSpan(new android.text.style.ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.colorAccent)), 0, str.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        noDebtsTextView.setText(str);
        noDebtsTextView.setTextSize(17);
        noDebtsTextView.setPadding((int) (16 * Resources.getSystem().getDisplayMetrics().density)
                , (int) (16 * Resources.getSystem().getDisplayMetrics().density)
                , (int) (16 * Resources.getSystem().getDisplayMetrics().density)
                , (int) (16 * Resources.getSystem().getDisplayMetrics().density));

        final SpannableStringBuilder str2 = new SpannableStringBuilder(getString(R.string.generic_no_payments));
        str2.setSpan(new android.text.style.ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.colorAccent)), 0, str2.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        noPaymentsTextView.setText(str2);
        noPaymentsTextView.setTextSize(17);
        noPaymentsTextView.setPadding((int) (16 * Resources.getSystem().getDisplayMetrics().density)
                , (int) (16 * Resources.getSystem().getDisplayMetrics().density)
                , (int) (16 * Resources.getSystem().getDisplayMetrics().density)
                , (int) (16 * Resources.getSystem().getDisplayMetrics().density));

        newPaymentButton = layout.findViewById(R.id.buttonMakePayment);
        newPaymentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newPaymentIntent = new Intent(getContext(), NewPaymentActivity.class);
                startActivityForResult(newPaymentIntent, NEW_PAYMENT);
            }
        });
        listener = new DataChangeListener() {
            @Override
            public void onDataChange() {
                mealExpenseData = mealDataManager.getData();
                if(mealExpenseData != null) {
                    initFilterSpinner();
                    initDebtsData();
                    initPaymentsData();
                    toggleProgressBar(false);
                } else {
                    toggleProgressBar(true);
                }
            }
        };
        return layout;
    }

    private void toggleProgressBar(boolean state) {
        if(state) {
            progressBar.setVisibility(View.VISIBLE);
            paymentsContainer.setVisibility(View.INVISIBLE);
            debtsContainer.setVisibility(View.INVISIBLE);
            paymentsHeader.setVisibility(View.GONE);
            debtsHeader.setVisibility(View.GONE);
            spinner.setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.GONE);
            paymentsContainer.setVisibility(View.VISIBLE);
            debtsContainer.setVisibility(View.VISIBLE);
            paymentsHeader.setVisibility(View.VISIBLE);
            debtsHeader.setVisibility(View.VISIBLE);
            spinner.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == NEW_PAYMENT) {
            if(resultCode == Activity.RESULT_OK){
                int from = data.getIntExtra("from",-1);
                int to = data.getIntExtra("to",-1);
                float amount = data.getFloatExtra("amount",-1);
                if(from != -1 && to != -1 && amount != -1) {
                    final Payment payment = new Payment(mealExpenseData.getMembers().get(from), mealExpenseData.getMembers().get(to), amount);
                    mealDataManager.addPayment(payment);
                    Snackbar snackbar = Snackbar.make(getActivity().findViewById(R.id.activity_main_coordinator_layout), R.string.payment_saved, Snackbar.LENGTH_LONG);
                    snackbar.setAction("UNDO", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mealDataManager.removePayment(payment);
                        }
                    });
                    snackbar.show();
                }else{
                    Toast.makeText(getContext(),"Error getting intent data", Toast.LENGTH_SHORT);
                }
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                Snackbar snackbar = Snackbar.make(getActivity().findViewById(R.id.activity_main_coordinator_layout), R.string.payment_creation_cancel, Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
    }

    private void initFilterSpinner(){
        if(spinner.getAdapter() == null || spinner.getAdapter().getCount() != mealExpenseData.getMembers().size()+1){
            ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(),  R.layout.spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            adapter.add(getString(R.string.all_data));
            adapter.addAll(mealExpenseData.getMemberNames());
            spinner.setAdapter(adapter);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    switch (i){
                        case 0:
                            activeMember = null;
                            break;
                        default:
                            i--;
                            activeMember = mealExpenseData.getMembers().get(i);
                    }
                    initDebtsData();
                    initPaymentsData();
                }
                @Override
                public void onNothingSelected(AdapterView<?> adapterView) { }
            });
        }
    }


    private void initDebtsData(){
        newPaymentButton.setEnabled(true);
        newPaymentButton.setVisibility(View.VISIBLE);
        List<Debt> allDebts = BalanceCalculator.calculateDebts(BalanceCalculator.getAllBalances(mealExpenseData));
        boolean noDebts = true;
        displayedDebtsList.clear();
        for (Debt debt : allDebts) {
            if(activeMember!=null) {
                if (debt.getDebtor().equals(activeMember)) {
                    displayedDebtsList.add(debt);
                    noDebts = false;
                }
            }else{
                displayedDebtsList.add(debt);
                noDebts = false;
            }
            debtsAdapter.notifyDataSetChanged();
        }
        if (noDebts){
            debtsContainer.setVisibility(View.GONE);
            noDebtsTextView.setVisibility(View.VISIBLE);
            newPaymentButton.setEnabled(false);
            newPaymentButton.setVisibility(View.GONE);
        } else {
            noDebtsTextView.setVisibility(View.GONE);
            debtsContainer.setVisibility(View.VISIBLE);
        }
    }


    private void initPaymentsData() {
        List<Payment> payments = mealExpenseData.getPayments();
        boolean noPayments = true;
        displayedPaymentsList.clear();
        for (Payment payment : payments) {
            if(activeMember!=null) {
                if (payment.getTo().equals(activeMember) || payment.getFrom().equals(activeMember)) {
                    displayedPaymentsList.add(payment);
                    noPayments = false;
                }
            }else{
                displayedPaymentsList.add(payment);
                noPayments = false;
            }
            paymentsAdapter.notifyDataSetChanged();
        }
        if (noPayments){
            noPaymentsTextView.setVisibility(View.VISIBLE);
        } else {
            noPaymentsTextView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onMethodCallback(final Object paymentObject, int action) {
        if(action == NEW_PAYMENT){
            final Payment payment = (Payment) paymentObject;
            Intent newPaymentIntent = new Intent(getContext(), NewPaymentActivity.class);
            newPaymentIntent.putExtra("to", mealExpenseData.getMembers().indexOf(payment.getTo()));
            newPaymentIntent.putExtra("from", mealExpenseData.getMembers().indexOf(payment.getFrom()));
            newPaymentIntent.putExtra("amount", payment.getAmount());
            startActivityForResult(newPaymentIntent, NEW_PAYMENT);
        }
        if(action == DELETE_PAYMENT){
            final Payment payment = (Payment) paymentObject;
            mealDataManager.removePayment(payment);
            Snackbar snackbar = Snackbar.make(getActivity().findViewById(R.id.activity_main_coordinator_layout), R.string.delete_payment, Snackbar.LENGTH_LONG);
            snackbar.setAction("UNDO", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mealDataManager.addPayment(payment);
                }
            });
            snackbar.show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mealDataManager.addDataChangeListener(listener);
    }

    @Override
    public void onPause() {
        super.onPause();
        mealDataManager.removeDataChangeListener(listener);
    }
}
