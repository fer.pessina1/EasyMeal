package com.pessinapps.easymeal.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.pessinapps.easymeal.ExpenseRecyclerViewAdapter;
import com.pessinapps.easymeal.R;
import com.pessinapps.easymeal.firebase.MealDataManager;
import com.pessinapps.easymeal.firebase.model.Expense;
import com.pessinapps.easymeal.firebase.model.MealExpenseData;
import com.pessinapps.easymeal.firebase.model.Person;
import com.pessinapps.easymeal.util.DataChangeListener;
import com.pessinapps.easymeal.util.RecyclerViewAdapterCallback;

import java.util.ArrayList;
import java.util.List;

public class ExpenseFragment extends Fragment implements RecyclerViewAdapterCallback {
    private MealExpenseData mealData;
    private Person activeMember;
    private RecyclerView expenseContainer;
    private ExpenseRecyclerViewAdapter expenseAdapter;
    private List<Expense> displayedExpensesList = new ArrayList<>();
    private TextView noExpenseTextView;
    private TextView debtHeader;
    private LinearLayout paymentsTitleLayout;
    private ProgressBar progressBar;
    private Spinner spinner;
    private Expense editExpense = null;
    private View editLayout = null;
    private static final int NEW_EXPENSE = 1;
    private static final int EDIT_EXPENSE = 2;
    private static final int DELETE_EXPENSE = 3;
    private MealDataManager mealDataManager;
    private DataChangeListener listener;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View layout = inflater.inflate(R.layout.expense_fragment, container, false);

        mealDataManager = MealDataManager.getInstance();
        mealData = mealDataManager.getData();

        spinner = layout.findViewById(R.id.filterSpinner);
        debtHeader = layout.findViewById(R.id.tv_debts_header);
        paymentsTitleLayout = layout.findViewById(R.id.payments_title_layout);
        progressBar = layout.findViewById(R.id.expenses_progress_bar);

        expenseContainer = layout.findViewById(R.id.rv_debts);
        expenseAdapter = new ExpenseRecyclerViewAdapter(getContext(), displayedExpensesList, this, activeMember);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        expenseContainer.setLayoutManager(mLayoutManager);
        DefaultItemAnimator animator = new DefaultItemAnimator();
        expenseContainer.setItemAnimator(animator);
        expenseContainer.setAdapter(expenseAdapter);
        noExpenseTextView = layout.findViewById(R.id.tv_no_expenses);

        Button newExpense = layout.findViewById(R.id.buttonAdd);
        newExpense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newPaymentIntent = new Intent(getContext(), NewExpenseActivity.class);
                startActivityForResult(newPaymentIntent, NEW_EXPENSE);
            }
        });
        listener = new DataChangeListener() {
            @Override
            public void onDataChange() {
                MealExpenseData newData = mealDataManager.getData();
                mealData = newData;
                if(newData != null) {
                    toggleProgressBar(false);
                    initFilterSpinner();
                } else {
                    toggleProgressBar(true);
                }
                mealData = newData;
            }
        };
        return layout;
    }

    private void toggleProgressBar(boolean state){
        if(state){
            progressBar.setVisibility(View.VISIBLE);
            spinner.setVisibility(View.GONE);
            debtHeader.setVisibility(View.GONE);
            paymentsTitleLayout.setVisibility(View.GONE);
            expenseContainer.setVisibility(View.GONE);
            noExpenseTextView.setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.GONE);
            spinner.setVisibility(View.VISIBLE);
            debtHeader.setVisibility(View.VISIBLE);
            paymentsTitleLayout.setVisibility(View.VISIBLE);
        }
    }

    private void initFilterSpinner(){
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), R.layout.spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.add(getString(R.string.all_data));
        adapter.addAll(mealData.getMemberNames());
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i){
                    case 0:
                        activeMember = null;
                        break;
                    default:
                        i--;
                        activeMember = mealData.getMembers().get(i);
                }
                initExpenses();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) { }
        });
    }


    private void initExpenses(){
        boolean noExpenses = true;
        displayedExpensesList.clear();
        for (Expense expense : mealData.getExpenses()) {
            if(activeMember!=null) {
                if (expense.getCreator().equals(activeMember)) {
                    displayedExpensesList.add(expense);
                    noExpenses = false;
                }
            }else{
                displayedExpensesList.add(expense);
                noExpenses = false;
            }
            expenseAdapter.notifyDataSetChanged();
        }
        if(noExpenses){
            expenseContainer.setVisibility(View.GONE);
            noExpenseTextView.setVisibility(View.VISIBLE);
            setNoExpenses();
        } else {
            expenseContainer.setVisibility(View.VISIBLE);
            noExpenseTextView.setVisibility(View.GONE);
        }

    }

    private void setNoExpenses(){
        if(activeMember == null) {
            final SpannableStringBuilder str = new SpannableStringBuilder(getString(R.string.generic_no_expenses));
            str.setSpan(new android.text.style.ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.colorAccent)), 0, str.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            noExpenseTextView.setText(str);
            noExpenseTextView.setTextSize(17);
            noExpenseTextView.setPadding((int) (16 * Resources.getSystem().getDisplayMetrics().density)
                    , (int) (16 * Resources.getSystem().getDisplayMetrics().density)
                    , (int) (16 * Resources.getSystem().getDisplayMetrics().density)
                    , (int) (16 * Resources.getSystem().getDisplayMetrics().density));
        }else{
            final SpannableStringBuilder str = new SpannableStringBuilder(getString(R.string.no_expenses) + " " + activeMember.getName() + ".");
            str.setSpan(new android.text.style.ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.colorAccent)), 0, str.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            noExpenseTextView.setText(str);
            noExpenseTextView.setTextSize(17);
            noExpenseTextView.setPadding((int) (16 * Resources.getSystem().getDisplayMetrics().density)
                    , (int) (16 * Resources.getSystem().getDisplayMetrics().density)
                    , (int) (16 * Resources.getSystem().getDisplayMetrics().density)
                    , (int) (16 * Resources.getSystem().getDisplayMetrics().density));
        }
    }

    @Override
    public void onMethodCallback(final Object expenseObject, int action) {
        final Expense expense = (Expense) expenseObject;
        if(action == EDIT_EXPENSE){
            editExpense = expense;
            Intent editExpenseIntent = new Intent(getContext(), NewExpenseActivity.class);
            editExpenseIntent.putExtra("creator", expense.getCreator().getName());
            editExpenseIntent.putExtra("concept", expense.getConcept());
            editExpenseIntent.putExtra("amount", expense.getAmount());
            ArrayList<String> beneficiariesList = new ArrayList<>();
            for (Person person : expense.getBeneficiaries()) {
                beneficiariesList.add(person.getName());
            }
            editExpenseIntent.putStringArrayListExtra("beneficiaries", beneficiariesList);
            startActivityForResult(editExpenseIntent, EDIT_EXPENSE);
        } else if (action == DELETE_EXPENSE){
            mealDataManager.removeExpense(expense);
            Snackbar snackbar = Snackbar.make(getActivity().findViewById(R.id.activity_main_coordinator_layout), R.string.delete_expense, Snackbar.LENGTH_LONG);
            snackbar.setAction(R.string.undo_snackbar, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mealDataManager.addExpense(expense);
                }
            });
            snackbar.show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == NEW_EXPENSE) {
            if(resultCode == Activity.RESULT_OK){
                int creatorInt = data.getIntExtra("creator",-1);
                String concept = data.getStringExtra("concept");
                float amount = data.getFloatExtra("amount",-1);
                List<String> beneficiariesString = data.getStringArrayListExtra("beneficiaries");

                if(creatorInt != -1 && concept != null && amount != -1 && beneficiariesString!=null) {
                    if (beneficiariesString.size() > 0) {
                        List<Person> beneficiaries = new ArrayList<>();
                        Person creator = mealData.getMembers().get(creatorInt);
                        for (String s : beneficiariesString) {
                            Person p = null;
                            for (Person person : mealData.getMembers()) {
                                if(person.getName().equals(s)){
                                    p = person;
                                    break;
                                }
                            }
                            if (p != null){
                                beneficiaries.add(p);
                            }
                        }
                        final Expense expense = new Expense(amount, concept, creator,beneficiaries);
                        mealDataManager.addExpense(expense);
                        Snackbar snackbar = Snackbar.make(getActivity().findViewById(R.id.activity_main_coordinator_layout), R.string.expense_saved, Snackbar.LENGTH_LONG);
                        snackbar.setAction(R.string.undo_snackbar, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                mealDataManager.removeExpense(expense);
                            }
                        });
                        snackbar.show();
                    }
                }else{
                    Toast.makeText(getContext(),"Error getting intent data", Toast.LENGTH_SHORT);
                }
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                Snackbar snackbar = Snackbar.make(getActivity().findViewById(R.id.activity_main_coordinator_layout), R.string.expense_creation_cancel, Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }else if (requestCode == EDIT_EXPENSE) {
            if(resultCode == Activity.RESULT_OK){
                int creatorInt = data.getIntExtra("creator",-1);
                String concept = data.getStringExtra("concept");
                float amount = data.getFloatExtra("amount",-1);
                List<String> beneficiariesString = data.getStringArrayListExtra("beneficiaries");

                if(creatorInt != -1 && concept != null && amount != -1 && beneficiariesString!=null) {
                    if (beneficiariesString.size() > 0) {
                        List<Person> beneficiaries = new ArrayList<>();
                        Person creator = mealData.getMembers().get(creatorInt);
                        for (String s : beneficiariesString) {
                            Person p = null;
                            for (Person person : mealData.getMembers()) {
                                if(person.getName().equals(s)){
                                    p = person;
                                    break;
                                }
                            }
                            if (p != null){
                                beneficiaries.add(p);
                            }
                        }
                        final Expense expense = new Expense(amount, concept, creator,beneficiaries);
                        mealDataManager.editExpense(editExpense,expense);
                        Snackbar snackbar = Snackbar.make(getActivity().findViewById(R.id.activity_main_coordinator_layout), R.string.expense_modified, Snackbar.LENGTH_LONG);
                        snackbar.setAction(R.string.undo_snackbar, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                mealDataManager.editExpense(expense, editExpense);
                            }
                        });
                        snackbar.show();
                    }
                }else{
                    Toast.makeText(getContext(),"Error getting intent data", Toast.LENGTH_SHORT);
                }
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                Snackbar snackbar = Snackbar.make(getActivity().findViewById(R.id.activity_main_coordinator_layout), R.string.expense_edition_cancel, Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mealDataManager.addDataChangeListener(listener);
    }

    @Override
    public void onPause() {
        super.onPause();
        mealDataManager.removeDataChangeListener(listener);
    }
}
