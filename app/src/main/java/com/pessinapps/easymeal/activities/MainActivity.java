package com.pessinapps.easymeal.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.pessinapps.easymeal.ExpenseAdapter;
import com.pessinapps.easymeal.firebase.AuthManager;
import com.pessinapps.easymeal.MapAdapter;
import com.pessinapps.easymeal.R;
import com.pessinapps.easymeal.firebase.MealDataManager;

public class MainActivity extends AppCompatActivity {

    private NavigationView mDrawerList;
    private Toolbar toolbar;
    private CoordinatorLayout mainLayout;
    private TabLayout tabLayout;
    private String[] activityTitles;
    private static int navItemIndex = 1;
    private DrawerLayout drawer;
    private ProgressBar loadingIndicator;
    private ViewPager viewPager;

    private static final String TAG_EXPENSES = "expenses";
    private static final String TAG_MAP = "map";
    public static String CURRENT_TAG = TAG_EXPENSES;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AuthManager.getInstance(this);

        toolbar = findViewById(R.id.toolbar_main);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_24px);
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mainLayout = findViewById(R.id.activity_main_coordinator_layout);
        tabLayout = findViewById(R.id.tab_layout_main);
        drawer = findViewById(R.id.drawer_layout);

        loadingIndicator = findViewById(R.id.loading_indicator);
        loadingIndicator.setVisibility(View.VISIBLE);

        activityTitles = getResources().getStringArray(R.array.nav_item_activity_titles);
        updateToolbar();

        viewPager = findViewById(R.id.view_pager_main);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.setOffscreenPageLimit(3);
        loadAdapter();
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);

        mDrawerList = findViewById(R.id.left_drawer);
        setUpNavigationView();
        mDrawerList.setCheckedItem(R.id.nav_expenses);

        Intent intent = getIntent();
        String uriString = intent.getDataString();
        if(uriString != null) {
            Uri uri = Uri.parse(uriString);
            final String key = uri.getQueryParameter("key");
            if (key != null) {
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.setTitle(R.string.alert_title); //Set Alert dialog title here
                alert.setMessage(R.string.alert_text); //Message here
                alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        MealDataManager.getInstance().joinMeal(key);
                    } // End of onClick(DialogInterface dialog, int whichButton)
                });
                alert.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.cancel();
                    }
                });
                AlertDialog alertDialog = alert.create();
                alertDialog.show();
            }
        }
    }

    private void loadAdapter(){
        final DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        Spinner toolbarSpinner = (Spinner) toolbar.getChildAt(0);
        mainLayout.animate().alpha(0).setDuration(150);
        AppBarLayout appBarLayout = findViewById(R.id.appBarLayout);
        appBarLayout.setExpanded(true, true);
        updateToolbar();
        switch (navItemIndex){
            case 1:
                toolbarSpinner.setVisibility(View.GONE);
                loadingIndicator.setVisibility(View.VISIBLE);
                tabLayout.setVisibility(View.VISIBLE);
                viewPager.setAdapter(new ExpenseAdapter(getSupportFragmentManager(),getResources().getStringArray(R.array.titles1)));
                viewPager.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadingIndicator.setVisibility(View.GONE);
                        toolbar.getMenu().clear();
                        MenuInflater inflater = getMenuInflater();
                        inflater.inflate(R.menu.toolbar_expenses_menu, toolbar.getMenu());
                        toolbar.setOverflowIcon(ContextCompat.getDrawable(getApplicationContext(),R.drawable.ic_3dot_24px));
                        drawer.closeDrawers();
                        mainLayout.animate().alpha(1).setDuration(600);
                    }
                },150);
                break;
            case 2:
                toolbarSpinner.setVisibility(View.VISIBLE);
                toolbar.getMenu().clear();
                tabLayout.setVisibility(View.GONE);
                loadingIndicator.setVisibility(View.VISIBLE);
                viewPager.setAdapter(new MapAdapter(getSupportFragmentManager(),getResources().getStringArray(R.array.titles2)));
                viewPager.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadingIndicator.setVisibility(View.GONE);
                        drawer.closeDrawers();
                        mainLayout.animate().alpha(1).setDuration(600);
                    }
                }, 150);
                break;
        }
    }

    private void setUpNavigationView() {
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        mDrawerList.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(@NonNull final MenuItem menuItem) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        String oldTag = CURRENT_TAG;
                        switch (menuItem.getItemId()) {
                            case R.id.nav_expenses:
                                navItemIndex = 1;
                                CURRENT_TAG = TAG_EXPENSES;
                                break;
                            case R.id.nav_map:
                                navItemIndex = 2;
                                CURRENT_TAG = TAG_MAP;
                                break;
                            case R.id.nav_logout:
                                AuthManager.getInstance().logout();
                                loadAdapter();
                                mDrawerList.setCheckedItem(R.id.nav_expenses);
                                break;
                            default:
                                navItemIndex = 0;
                        }
                        if(!oldTag.equals(CURRENT_TAG)) {
                            //Checking if the item is in checked state or not, if not make it in checked state
                            if (menuItem.isChecked()) {
                                menuItem.setChecked(false);
                            } else {
                                menuItem.setChecked(true);
                            }
                            menuItem.setChecked(true);
                            loadAdapter();
                        }else{
                            drawer.closeDrawers();
                        }
                    }
                }, 50);
                return true;
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                drawer.openDrawer(GravityCompat.START);
                return true;
            case R.id.get_key_menu_item:

                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.putExtra(Intent.EXTRA_TEXT, "https://easy.meal/shareKey?key=" + MealDataManager.getInstance().getExpenseKey());
                startActivity(Intent.createChooser(share, "Share Meal Key"));

                return true;
            case R.id.join_group_menu_item:
                final Context context = getApplicationContext();
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.setTitle("Join meal"); //Set Alert dialog title here
                alert.setMessage("Enter the key for the meal you want to join"); //Message here

                // Set an EditText view to get user input
                final EditText input = new EditText(context);
                alert.setView(input);

                alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //You will get as string input data in this variable.
                        // here we convert the input to a string and show in a toast.
                        String srt = input.getEditableText().toString();
                        MealDataManager.getInstance().joinMeal(srt);
                    } // End of onClick(DialogInterface dialog, int whichButton)
                }); //End of alert.setPositiveButton
                alert.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // Canceled.
                        dialog.cancel();
                    }
                }); //End of alert.setNegativeButton
                AlertDialog alertDialog = alert.create();
                alertDialog.show();
                return true;
            case R.id.clear_menu_item:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(R.string.clear_dialog_message);
                builder.setTitle(R.string.clear_dialog_title);
                builder.setPositiveButton(R.string.clear_all, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                MealDataManager.getInstance().leaveMeal();
                                MealDataManager.getInstance().createMeal();
                            }
                        }, 700);

                    }
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
                AlertDialog dialog = builder.create();
                if(dialog.getWindow() != null) {
                    dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                }
                dialog.show();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void updateToolbar() {
        toolbar.setTitle(activityTitles[navItemIndex]);
    }

    @Override
    protected void onPause() {
        AuthManager.getInstance(this).detachListener();
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        AuthManager.getInstance(this).attachListener();
    }

    @Override
    public void onBackPressed(){
        if(drawer.isDrawerOpen(findViewById(R.id.left_drawer))){
            drawer.closeDrawer(findViewById(R.id.left_drawer));
        }else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == AuthManager.RC_SIGN_IN){
            if(resultCode == RESULT_OK){
                Toast.makeText(this, "Signed in!", Toast.LENGTH_SHORT).show();
            }else if(resultCode == RESULT_CANCELED){
                Toast.makeText(this, "Sign in canceled.", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }
}
