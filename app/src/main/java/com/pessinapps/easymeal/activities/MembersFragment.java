package com.pessinapps.easymeal.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.pessinapps.easymeal.R;

import com.pessinapps.easymeal.firebase.MealDataManager;
import com.pessinapps.easymeal.firebase.model.Expense;
import com.pessinapps.easymeal.firebase.model.MealExpenseData;
import com.pessinapps.easymeal.firebase.model.Payment;
import com.pessinapps.easymeal.firebase.model.Person;
import com.pessinapps.easymeal.util.DataChangeListener;


public class MembersFragment extends Fragment {
    private static final int NEW_MEMBER = 1;
    private MealExpenseData mealExpenseData;
    private LinearLayout membersContainer;
    private TextView memberCount;
    private FloatingActionButton floatingActionButton;
    private MealDataManager mealDataManager;
    private DataChangeListener listener;
    private ProgressBar progressBar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View layout = inflater.inflate(R.layout.members_fragment, container, false);

        mealDataManager = MealDataManager.getInstance();

        membersContainer = layout.findViewById(R.id.ll_members_list);
        floatingActionButton = layout.findViewById(R.id.addMemberButton);
        memberCount = layout.findViewById(R.id.groupInfo);
        progressBar = layout.findViewById(R.id.members_progress_bar);
        //loadMembers();
        initFAB();
        listener = new DataChangeListener() {
            @Override
            public void onDataChange() {
                mealExpenseData = mealDataManager.getData();
                if(mealExpenseData != null) {
                    toggleProgressBar(false);
                    loadMembers();
                } else {
                    toggleProgressBar(true);
                }
            }
        };
        return layout;
    }

    private void initFAB() {
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newPaymentIntent = new Intent(getContext(), NewMemberActivity.class);
                startActivityForResult(newPaymentIntent, NEW_MEMBER);
            }
        });
    }

    private void toggleProgressBar(boolean state){
        if(state) {
            progressBar.setVisibility(View.VISIBLE);
            membersContainer.setVisibility(View.INVISIBLE);
            floatingActionButton.setVisibility(View.INVISIBLE);
            memberCount.setVisibility(View.INVISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
            membersContainer.setVisibility(View.VISIBLE);
            floatingActionButton.setVisibility(View.VISIBLE);
            memberCount.setVisibility(View.VISIBLE);
        }
    }

    private void loadMembers() {
        membersContainer.removeAllViews();
        if(mealExpenseData.getMembers().size() != 1) {
            String members = " " + getContext().getString(R.string.members1);
            final SpannableStringBuilder str = new SpannableStringBuilder(mealExpenseData.getMembers().size() + members);
            str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, str.length() - members.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            str.setSpan(new android.text.style.ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.colorAccent)), 0, str.length() - members.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            memberCount.setText(str);
        }else {
            String members = " " + getContext().getString(R.string.member);
            final SpannableStringBuilder str = new SpannableStringBuilder(mealExpenseData.getMembers().size() + members);
            str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, str.length() - members.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            str.setSpan(new android.text.style.ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.colorAccent)), 0, str.length() - members.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            memberCount.setText(str);
        }
        if(mealExpenseData.getMembers().size() == 0){
            TextView textView = new TextView(getContext());
            final SpannableStringBuilder str = new SpannableStringBuilder(getString(R.string.no_members));
            str.setSpan(new android.text.style.ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.colorAccent)), 0, str.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            textView.setText(str);
            textView.setTextSize(17);
            textView.setPadding((int) (16 * Resources.getSystem().getDisplayMetrics().density)
                    , (int) (16 * Resources.getSystem().getDisplayMetrics().density)
                    , (int) (16 * Resources.getSystem().getDisplayMetrics().density)
                    , (int) (16 * Resources.getSystem().getDisplayMetrics().density));
            membersContainer.addView(textView);
        }else {
            for (Person member : mealExpenseData.getMembers()) {
                final ViewGroup nullParent = null;
                View memberLayout = LayoutInflater.from(getContext()).inflate(R.layout.member_list_item, nullParent);
                TextView main = memberLayout.findViewById(R.id.tv_member_list);
                TextView details = memberLayout.findViewById(R.id.tv_member_details);
                ImageButton delete = memberLayout.findViewById(R.id.bt_delete_member);
                delete.setTag(R.id.member, member);
                delete.setTag(R.id.layout, memberLayout);
                main.setText(member.getName());
                int expensesCount = 0;
                int paymentsCount = 0;
                for (Expense expense : mealExpenseData.getExpenses()) {
                    if (expense.getCreator().equals(member))
                        expensesCount++;
                }
                for (Payment payment : mealExpenseData.getPayments()) {
                    if (payment.getFrom().equals(member) || payment.getTo().equals(member))
                        paymentsCount++;
                }
                String sDetails = getString(R.string.expenses2) + " " + expensesCount
                        + " - " + getString(R.string.payments2) + " " + paymentsCount;
                details.setText(sDetails);
                membersContainer.addView(memberLayout);
                delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final Person member1 = (Person) view.getTag(R.id.member);
                        final View layout = (View) view.getTag(R.id.layout);
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setMessage(R.string.delete_dialog_message);
                        builder.setTitle(R.string.delete_dialog_title);
                        builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                mealDataManager.removeMember(member1);
                            }
                        });
                        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        });
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                });
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mealDataManager.addDataChangeListener(listener);
    }

    @Override
    public void onPause() {
        super.onPause();
        mealDataManager.removeDataChangeListener(listener);
    }
}
