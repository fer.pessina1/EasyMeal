package com.pessinapps.easymeal.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.CompoundButtonCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.pessinapps.easymeal.R;
import com.pessinapps.easymeal.firebase.MealDataManager;
import com.pessinapps.easymeal.firebase.model.MealExpenseData;
import com.pessinapps.easymeal.firebase.model.Person;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class NewExpenseActivity extends AppCompatActivity {

    private MealExpenseData mealExpenseData;
    private Spinner creatorSpinner;
    private EditText conceptEditText;
    private EditText amountEditText;
    private Button saveButton;
    private Button cancelButton;
    private ImageButton selectMultipleButton;
    private LinearLayout beneficiariesListView;
    private Set<Person> beneficiaries = new HashSet<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_expense);

        String creator = getIntent().getStringExtra("creator");
        String concept = getIntent().getStringExtra("concept");
        float amount = getIntent().getFloatExtra("amount",-1);
        List<String> beneficiariesString = getIntent().getStringArrayListExtra("beneficiaries");

        mealExpenseData = MealDataManager.getInstance().getData();

        creatorSpinner = findViewById(R.id.spinner_creator);
        conceptEditText = findViewById(R.id.et_concept);
        conceptEditText.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        amountEditText = findViewById(R.id.et_amount);
        amountEditText.setImeOptions(EditorInfo.IME_ACTION_DONE);
        cancelButton = findViewById(R.id.bt_cancel);
        saveButton = findViewById(R.id.bt_save);
        beneficiariesListView = findViewById(R.id.ll_members_list);
        selectMultipleButton = findViewById(R.id.ib_beneficiaries_header);

        initEditTexts();
        initSpinners();
        initButtons();

        if(creator != null && concept != null && amount != -1 && beneficiariesString!=null){
            if(beneficiariesString.size()>0){
                //Data complete, set defaults
                creatorSpinner.setSelection(mealExpenseData.getMemberNames().indexOf(creator)+1);
                conceptEditText.setText(concept);
                amountEditText.setText(String.valueOf(amount));
                for(String s : beneficiariesString){
                    Person p = null;
                    for (Person person : mealExpenseData.getMembers()) {
                        if(person.getName().equals(s)) {
                            p = person;
                            break;
                        }
                    }
                    if(p != null) {
                        beneficiaries.add(p);
                    }
                }
            }
        }

        initCheckBoxList();
    }

    private void initEditTexts() {
        //INIT EDIT TEXT LISTENER
        amountEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                enableDisableSaveButton();
            }
            @Override
            public void afterTextChanged(Editable editable) {}
        });

        //INIT EDIT TEXT LISTENER
        conceptEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                enableDisableSaveButton();
            }
            @Override
            public void afterTextChanged(Editable editable) {}
        });
    }

    private void initSpinners() {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,R.layout.spinner_item2);
        adapter.add("Select member");
        adapter.addAll(mealExpenseData.getMemberNames());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        creatorSpinner.setAdapter(adapter);
        creatorSpinner.setSelection(0,false);
        // INIT SPINNER LISTENERS
        AdapterView.OnItemSelectedListener listener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                enableDisableSaveButton();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        };
        creatorSpinner.setOnItemSelectedListener(listener);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                creatorSpinner.performClick();
            }
        },50);
    }

    public void initButtons(){
        //INIT BUTTONS
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_CANCELED);
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                finish();
            }
        });
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                ArrayList<String> beneficiariesString = new ArrayList<>();
                for (Person person : beneficiaries) {
                    beneficiariesString.add(person.getName());
                }
                returnIntent.putExtra("creator",creatorSpinner.getSelectedItemPosition()-1);
                returnIntent.putExtra("amount", Float.parseFloat(amountEditText.getText().toString()));
                returnIntent.putExtra("concept",conceptEditText.getText().toString());
                returnIntent.putStringArrayListExtra("beneficiaries",beneficiariesString);
                setResult(Activity.RESULT_OK, returnIntent);
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                finish();
            }
        });
        selectMultipleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(beneficiaries.size()>0){
                    //get references to checkboxes??
                    for (int i = 0; i < beneficiariesListView.getChildCount(); i++) {
                        CheckBox checkBox = (CheckBox) beneficiariesListView.getChildAt(i);
                        if(checkBox.isChecked()) {
                            checkBox.setChecked(false);
                            Person person = (Person) checkBox.getTag(R.string.person_tag);
                            beneficiaries.remove(person);
                        }
                    }
                }else {
                    for (int i = 0; i < beneficiariesListView.getChildCount(); i++) {
                        CheckBox checkBox = (CheckBox) beneficiariesListView.getChildAt(i);
                        if(!checkBox.isChecked()) {
                            checkBox.setChecked(true);
                            Person person = (Person) checkBox.getTag(R.string.person_tag);
                            beneficiaries.add(person);
                        }
                    }
                }
                updateSelectButtonIcon();
            }
        });
    }

    public void initCheckBoxList(){
        for (Person person : mealExpenseData.getMembers()) {
            final CheckBox checkBox = new CheckBox(this);
            checkBox.setText(person.getName());
            if(beneficiaries.contains(person))
                checkBox.setChecked(true);
            checkBox.setTag(R.string.person_tag, person);
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    Person person1 = (Person) compoundButton.getTag(R.string.person_tag);
                    if(b){
                        beneficiaries.add(person1);
                    }else {
                        beneficiaries.remove(person1);
                    }
                    updateSelectButtonIcon();
                    enableDisableSaveButton();
                }
            });
            checkBox.setPadding((int) (4 * Resources.getSystem().getDisplayMetrics().density)
                    , (int) (8 * Resources.getSystem().getDisplayMetrics().density)
                    , (int) (4 * Resources.getSystem().getDisplayMetrics().density)
                    , (int) (8 * Resources.getSystem().getDisplayMetrics().density));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins((int) (16 * Resources.getSystem().getDisplayMetrics().density)
                    , (int) (2 * Resources.getSystem().getDisplayMetrics().density)
                    , (int) (16 * Resources.getSystem().getDisplayMetrics().density)
                    , (int) (2 * Resources.getSystem().getDisplayMetrics().density));
            checkBox.setLayoutParams(params);
            checkBox.setTextColor(ContextCompat.getColor(getApplicationContext(),R.color.colorSecondaryText));
            checkBox.setTextSize(16);

            int states[][] = {{android.R.attr.state_checked}, {}};
            int colors[] = {ContextCompat.getColor(getApplicationContext(),R.color.colorAccent), ContextCompat.getColor(getApplicationContext(),R.color.colorSecondaryText)};
            CompoundButtonCompat.setButtonTintList(checkBox, new ColorStateList(states, colors));
            beneficiariesListView.addView(checkBox);
            updateSelectButtonIcon();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void enableDisableSaveButton(){
        if(creatorSpinner.getSelectedItemPosition() > 0
                && conceptEditText.getText().length() > 0
                && amountEditText.getText().length() > 0
                && beneficiaries.size() > 0){
            saveButton.setClickable(true);
            saveButton.setTextColor(ContextCompat.getColor(this,R.color.colorWhite));
        }else {
            saveButton.setClickable(false);
            saveButton.setTextColor(ContextCompat.getColor(this,R.color.colorSecondaryText));
        }
    }

    public void updateSelectButtonIcon(){
        if(beneficiaries.size()>0){
            selectMultipleButton.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.ic_cancel2_24px));
        }else {
            selectMultipleButton.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.ic_select_all_24px));
        }
    }
}
