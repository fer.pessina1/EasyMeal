package com.pessinapps.easymeal.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.CompoundButtonCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.pessinapps.easymeal.R;
import com.pessinapps.easymeal.firebase.MealDataManager;
import com.pessinapps.easymeal.firebase.model.Expense;
import com.pessinapps.easymeal.firebase.model.MealExpenseData;
import com.pessinapps.easymeal.firebase.model.Person;

import java.util.HashSet;
import java.util.Set;

public class NewMemberActivity extends AppCompatActivity {
    private MealExpenseData mealExpenseData;
    private EditText etName;
    private Button saveButton;
    private Button cancelButton;
    private LinearLayout expensesLayout;
    private LinearLayout expenseHeader;
    private ImageButton selectMultipleButton;
    private Set<Expense> expensesList = new HashSet<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_member);

        mealExpenseData = MealDataManager.getInstance().getData();

        etName = findViewById(R.id.et_name);
        saveButton = findViewById(R.id.bt_save);
        cancelButton = findViewById(R.id.bt_cancel);
        expensesLayout = findViewById(R.id.ll_expenses);
        selectMultipleButton = findViewById(R.id.ib_beneficiaries_header);
        expenseHeader = findViewById(R.id.expense_header);

        initEditTexts();
        initCheckBoxList();
        initButtons();
        enableDisableSaveButton();
    }

    public void initButtons(){
        //INIT BUTTONS
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Person member = new Person(etName.getText().toString(),"");
//                mealExpenseData.getMembers().add(member);
//                for (Expense expense : mealExpenseData.getExpenses()) {
//                    if(expensesList.contains(expense)){
//                        expense.getBeneficiaries().add(member);
//                    }
//                }
//                ExpenseManager.getInstance().saveData(mealExpenseData);
                MealDataManager.getInstance().addMember(member, expensesList);
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_OK, returnIntent);
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(etName.getWindowToken(), 0);
                finish();
            }
        });
        selectMultipleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(expensesList.size()>0){
                    //get references to checkboxes??
                    for (int i = 0; i < expensesLayout.getChildCount(); i++) {
                        CheckBox checkBox = (CheckBox) expensesLayout.getChildAt(i);
                        if(checkBox.isChecked()) {
                            checkBox.setChecked(false);
                            Expense expense = (Expense) checkBox.getTag();
                            expensesList.remove(expense);
                        }
                    }
                }else {
                    for (int i = 0; i < expensesLayout.getChildCount(); i++) {
                        CheckBox checkBox = (CheckBox) expensesLayout.getChildAt(i);
                        if(!checkBox.isChecked()) {
                            checkBox.setChecked(true);
                            Expense expense = (Expense) checkBox.getTag();
                            expensesList.add(expense);
                        }
                    }
                }
                updateSelectButtonIcon();
            }
        });
    }


    public void initCheckBoxList(){
        if(mealExpenseData.getExpenses().size()==0){
            expenseHeader.setVisibility(View.GONE);
        }else {
            for (Expense expense : mealExpenseData.getExpenses()) {
                final CheckBox checkBox = new CheckBox(this);
                checkBox.setText(expense.getConcept());
                checkBox.setTag(expense);
                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        Expense expense1 = (Expense) compoundButton.getTag();
                        if (b) {
                            expensesList.add(expense1);
                        } else {
                            if (expensesList.contains(expense1))
                                expensesList.remove(expense1);
                        }
                        updateSelectButtonIcon();
                        enableDisableSaveButton();
                    }
                });
                checkBox.setPadding((int) (4 * Resources.getSystem().getDisplayMetrics().density)
                        , (int) (8 * Resources.getSystem().getDisplayMetrics().density)
                        , (int) (4 * Resources.getSystem().getDisplayMetrics().density)
                        , (int) (8 * Resources.getSystem().getDisplayMetrics().density));

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.setMargins((int) (16 * Resources.getSystem().getDisplayMetrics().density)
                        , (int) (2 * Resources.getSystem().getDisplayMetrics().density)
                        , (int) (16 * Resources.getSystem().getDisplayMetrics().density)
                        , (int) (2 * Resources.getSystem().getDisplayMetrics().density));
                checkBox.setLayoutParams(params);
                checkBox.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorSecondaryText));
                checkBox.setTextSize(16);

                int states[][] = {{android.R.attr.state_checked}, {}};
                int colors[] = {ContextCompat.getColor(getApplicationContext(), R.color.colorAccent), ContextCompat.getColor(getApplicationContext(), R.color.colorSecondaryText)};
                CompoundButtonCompat.setButtonTintList(checkBox, new ColorStateList(states, colors));
                expensesLayout.addView(checkBox);
                updateSelectButtonIcon();
            }
        }
    }

    private void initEditTexts() {
        //INIT EDIT TEXT LISTENER
        etName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                enableDisableSaveButton();
            }
            @Override
            public void afterTextChanged(Editable editable) {}
        });
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    public void updateSelectButtonIcon(){
        if(expensesList.size()>0){
            selectMultipleButton.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.ic_cancel2_24px));
        }else {
            selectMultipleButton.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.ic_select_all_24px));
        }
    }

    public void enableDisableSaveButton(){
        if(etName.getText().length() > 0){
            saveButton.setClickable(true);
            saveButton.setTextColor(ContextCompat.getColor(this,R.color.colorWhite));
        }else {
            saveButton.setClickable(false);
            saveButton.setTextColor(ContextCompat.getColor(this,R.color.colorSecondaryText));
        }
    }
}
