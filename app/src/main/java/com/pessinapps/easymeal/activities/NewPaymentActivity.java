package com.pessinapps.easymeal.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.pessinapps.easymeal.R;
import com.pessinapps.easymeal.firebase.MealDataManager;
import com.pessinapps.easymeal.firebase.model.MealExpenseData;

public class NewPaymentActivity extends AppCompatActivity {

    Spinner fromSpinner;
    Spinner toSpinner;
    MealExpenseData mealExpenseData;
    Button saveButton;
    EditText amountEditText;
    Button cancelButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_new_payment);

        int from = getIntent().getIntExtra("from",-1);
        int to = getIntent().getIntExtra("to",-1);
        float amount = getIntent().getFloatExtra("amount",-1);

        mealExpenseData = MealDataManager.getInstance().getData();

        fromSpinner = findViewById(R.id.spinner_from);
        toSpinner = findViewById(R.id.spinner_to);
        saveButton = findViewById(R.id.bt_save);
        amountEditText = findViewById(R.id.et_amount);
        cancelButton = findViewById(R.id.bt_cancel);
        // INIT SPINNER ADAPTERS
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this,R.layout.spinner_item2);
        adapter.add("Select member");
        adapter.addAll(mealExpenseData.getMemberNames());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fromSpinner.setAdapter(adapter);
        toSpinner.setAdapter(adapter);


        if(from != -1 && to != -1 && amount != -1){
            fromSpinner.setSelection(from+1);
            toSpinner.setSelection(to+1);
            amountEditText.setText(String.valueOf(amount));
        }

        // INIT SPINNER LISTENERS
        AdapterView.OnItemSelectedListener listener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                enableDisableSaveButton();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        };
        fromSpinner.setOnItemSelectedListener(listener);
        toSpinner.setOnItemSelectedListener(listener);

        //INIT EDIT TEXT LISTENER
        amountEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                enableDisableSaveButton();
            }
            @Override
            public void afterTextChanged(Editable editable) {}
        });

        //INIT BUTTONS
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("from",fromSpinner.getSelectedItemPosition()-1);
                returnIntent.putExtra("to",toSpinner.getSelectedItemPosition()-1);
                returnIntent.putExtra("amount", Float.parseFloat(amountEditText.getText().toString()));
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        });
    }

    private void enableDisableSaveButton(){
        if(fromSpinner.getSelectedItemPosition() != toSpinner.getSelectedItemPosition()
                && toSpinner.getSelectedItemPosition() > 0
                && fromSpinner.getSelectedItemPosition() > 0
                && amountEditText.getText().length() > 0){
            saveButton.setClickable(true);
            saveButton.setTextColor(ContextCompat.getColor(this,R.color.colorWhite));
        }else {
            saveButton.setClickable(false);
            saveButton.setTextColor(ContextCompat.getColor(this,R.color.colorSecondaryText));
        }
    }
}
