package com.pessinapps.easymeal.activities;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.pessinapps.easymeal.R;

import com.pessinapps.easymeal.firebase.MealDataManager;
import com.pessinapps.easymeal.firebase.model.Debt;
import com.pessinapps.easymeal.firebase.model.Expense;
import com.pessinapps.easymeal.firebase.model.MealExpenseData;
import com.pessinapps.easymeal.firebase.model.Person;
import com.pessinapps.easymeal.util.BalanceCalculator;
import com.pessinapps.easymeal.util.DataChangeListener;

import java.util.ArrayList;
import java.util.List;


public class OverviewFragment extends Fragment {

    private MealExpenseData mealExpenseData;
    private Spinner groupSpinner;
    private Person selectedPerson;
    private TextView groupInfo;
    private TextView expensesTotal;
    private TextView expensesYou;
    private TextView expensesQuantity;
    private TextView textViewSpinner;
    private LinearLayout debtLayout;
    private ProgressBar overviewProgressBar;
    private Button goToMembers;
    private MealDataManager mealDataManager;
    private DataChangeListener listener;
    private Button goToExpenses;
    private Button goToDebts;
    private LinearLayout expensesTitleLayout;
    private LinearLayout debtTitleLayout;
    private  List<Expense> displayedExpenses = new ArrayList<>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View layout = inflater.inflate(R.layout.overview_fragment, container, false);

        mealDataManager = MealDataManager.getInstance();
        mealExpenseData = mealDataManager.getData();


        groupInfo = layout.findViewById(R.id.groupInfo);
        expensesTotal = layout.findViewById(R.id.tv_exp_overview_total);
        expensesYou = layout.findViewById(R.id.tv_exp_overview_you);
        expensesQuantity = layout.findViewById(R.id.tv_exp_overview_exp_no);
        debtLayout = layout.findViewById(R.id.ll_debts_overview);

        groupSpinner = layout.findViewById(R.id.groupSpinner);
        textViewSpinner = layout.findViewById(R.id.textViewSpinner);
        goToMembers = layout.findViewById(R.id.bt_view_members);
        goToExpenses = layout.findViewById(R.id.bt_view_expenses);
        goToDebts = layout.findViewById(R.id.bt_view_debts);

        overviewProgressBar = layout.findViewById(R.id.overview_progress_bar);
        debtTitleLayout = layout.findViewById(R.id.debts_overview_title_layout);
        expensesTitleLayout = layout.findViewById(R.id.expenses_overview_title_layout);


        groupSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(mealExpenseData != null) {
                    selectedPerson = mealExpenseData.getMembers().get(groupSpinner.getSelectedItemPosition());
                    updateExpensesOverview(mealExpenseData);
                    updateDebtOverview(mealExpenseData);
                } else {
                    toggleProgressBar(true);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        goToMembers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewPager pager = getActivity().findViewById(R.id.view_pager_main);
                pager.setCurrentItem(3,true);
            }
        });
        goToExpenses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewPager pager = getActivity().findViewById(R.id.view_pager_main);
                pager.setCurrentItem(1,true);
            }
        });
        goToDebts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewPager pager = getActivity().findViewById(R.id.view_pager_main);
                pager.setCurrentItem(2,true);
            }
        });
        listener = new DataChangeListener() {
            @Override
            public void onDataChange() {
                MealExpenseData newData = mealDataManager.getData();
                if(newData != null) {
                    toggleProgressBar(false);

                    updateGroupHeader(newData);
                    updateExpensesOverview(newData);
                    updateDebtOverview(newData);

                } else {
                    toggleProgressBar(true);
                }
                mealExpenseData = newData;
            }
        };
        return layout;
    }

    private void toggleProgressBar(boolean state) {
        if(state) {
            overviewProgressBar.setVisibility(View.VISIBLE);
            debtLayout.setVisibility(View.INVISIBLE);
            debtTitleLayout.setVisibility(View.INVISIBLE);
            expensesTitleLayout.setVisibility(View.INVISIBLE);
            goToMembers.setVisibility(View.GONE);
            groupSpinner.setVisibility(View.GONE);
        } else {
            overviewProgressBar.setVisibility(View.GONE);
            debtLayout.setVisibility(View.VISIBLE);
            debtTitleLayout.setVisibility(View.VISIBLE);
            expensesTitleLayout.setVisibility(View.VISIBLE);
            goToMembers.setVisibility(View.VISIBLE);
            groupSpinner.setVisibility(View.VISIBLE);
        }
    }

    private void updateGroupHeader(MealExpenseData data){
        if(data.getMembers().size()>0) {
            textViewSpinner.setVisibility(View.VISIBLE);
            groupSpinner.setVisibility(View.VISIBLE);
            goToMembers.setVisibility(View.GONE);
            goToDebts.setVisibility(View.VISIBLE);
            goToExpenses.setVisibility(View.VISIBLE);
            ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), R.layout.spinner_item, data.getMemberNames());
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            groupSpinner.setAdapter(adapter);
            if (data.getMembers().size() > 1) {
                String members = " " + getContext().getString(R.string.members1);
                final SpannableStringBuilder str = new SpannableStringBuilder(data.getMembers().size() + members);
                str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, str.length() - members.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                str.setSpan(new android.text.style.ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.colorAccent)), 0, str.length() - members.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                groupInfo.setText(str);
            } else {
                String members = " " + getContext().getString(R.string.member);
                final SpannableStringBuilder str = new SpannableStringBuilder(data.getMembers().size() + members);
                str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, str.length() - members.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                str.setSpan(new android.text.style.ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.colorAccent)), 0, str.length() - members.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                groupInfo.setText(str);
            }
            if(selectedPerson == null) {
                selectedPerson = data.getMembers().get(groupSpinner.getSelectedItemPosition());
            } else {
                groupSpinner.setSelection(data.getMembers().indexOf(selectedPerson));
            }
        }else{
            groupInfo.setText("No members added.");
            textViewSpinner.setVisibility(View.GONE);
            goToMembers.setVisibility(View.VISIBLE);
            goToDebts.setVisibility(View.GONE);
            goToExpenses.setVisibility(View.GONE);
        }
    }

    private void updateExpensesOverview(MealExpenseData data){
        if(data.getExpenses().size()>0) {
            //goToExpenses.setText(R.string.view_all_expenses);
            String total1 = getContext().getString(R.string.total1);
            float totalExpenses = 0;
            for (Expense expense : data.getExpenses()) {
                totalExpenses += expense.getAmount();
            }
            String total2 = getContext().getString(R.string.total2);
            final SpannableStringBuilder str = new SpannableStringBuilder(total1 + " $" + totalExpenses + " " + total2);
            str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), total1.length(), str.length() - total2.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            str.setSpan(new android.text.style.ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.colorAccent)), total1.length(), str.length() - total2.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            String yourShare = " $" + String.format(java.util.Locale.US,"%.2f", data.getMemberShare(selectedPerson));
            str.append(yourShare);
            str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), str.length() - yourShare.length(), str.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            str.setSpan(new android.text.style.ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.colorAccent)), str.length() - yourShare.length(), str.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            str.append(".");
            expensesTotal.setText(str);

            String you1 = getContext().getString(R.string.you1);
            String yourExpenses = " $" + data.getMemberExpenseTotal(selectedPerson);
            final SpannableStringBuilder str2 = new SpannableStringBuilder(you1 + yourExpenses);
            str2.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), str2.length() - yourExpenses.length(), str2.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            str2.setSpan(new android.text.style.ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.colorAccent)), str2.length() - yourExpenses.length(), str2.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            str2.append(".");
            expensesYou.setText(str2);

            String quant1 = getContext().getString(R.string.quant1);
            final SpannableStringBuilder str3 = new SpannableStringBuilder(data.getExpenses().size() + " " + quant1);
            str3.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, str3.length() - quant1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            str3.setSpan(new android.text.style.ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.colorAccent)), 0, str3.length() - quant1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            expensesQuantity.setText(str3);

            expensesYou.setVisibility(View.VISIBLE);
            expensesQuantity.setVisibility(View.VISIBLE);
        }else {
            expensesYou.setVisibility(View.GONE);
            expensesQuantity.setVisibility(View.GONE);
            //goToExpenses.setText(R.string.add_expense);
            final SpannableStringBuilder str = new SpannableStringBuilder(getString(R.string.generic_no_expenses));
            str.setSpan(new android.text.style.ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.colorAccent)), 0, str.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            expensesTotal.setText(str);
        }
    }

    public void updateDebtOverview(MealExpenseData data){
        List<Debt> allDebts = BalanceCalculator.calculateDebts(BalanceCalculator.getAllBalances(data));
        debtLayout.removeAllViews();
        boolean noDebts = true;
        for (Debt debt : allDebts) {
            Person debtor = debt.getDebtor();
            Person creditor = debt.getCreditor();
            Float amount = debt.getAmount();
            TextView textView = new TextView(getContext());
            textView.setTextSize(17);
            textView.setGravity(Gravity.CENTER_VERTICAL);
            if(debtor.equals(selectedPerson)){
                String debtor1 = getContext().getString(R.string.debtor1);
                debtor1 += " ";
                String amountString = " $" + String.format(java.util.Locale.US,"%.2f", amount);
                String to = " "+getContext().getString(R.string.to) + " ";
                final SpannableStringBuilder str = new SpannableStringBuilder(debtor1+amountString+to+creditor.getName());
                str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), debtor1.length(), debtor1.length()+amountString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                str.setSpan(new android.text.style.ForegroundColorSpan(ContextCompat.getColor(getContext(),R.color.colorAccent)), debtor1.length(), debtor1.length()+amountString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                str.append(".");
                textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_arrow_forward_24px,0,0,0);
                textView.setCompoundDrawablePadding((int) (8 * Resources.getSystem().getDisplayMetrics().density));
                textView.setText(str);
            }else if (creditor.equals(selectedPerson)){
                String creditor1 = " " + getContext().getString(R.string.creditor1) + " ";
                String amountString = " $" + String.format(java.util.Locale.US,"%.2f", amount);
                final SpannableStringBuilder str = new SpannableStringBuilder(debtor.getName()+creditor1+amountString);
                str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), str.length()-amountString.length(), str.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                str.setSpan(new android.text.style.ForegroundColorSpan(ContextCompat.getColor(getContext(),R.color.colorAccent)), str.length()-amountString.length(), str.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                str.append(".");
                textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_arrow_back_24px,0,0,0);
                textView.setCompoundDrawablePadding((int) (8 * Resources.getSystem().getDisplayMetrics().density));
                textView.setText(str);
            }else {
                String gen1 = " " + getContext().getString(R.string.owes) + " ";
                String amountString = " $" + String.format(java.util.Locale.US,"%.2f", amount);
                String to = " "+ getContext().getString(R.string.to) + " ";
                final SpannableStringBuilder str = new SpannableStringBuilder(debtor.getName() + gen1 + amountString + to + creditor.getName());
                str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), debtor.getName().length() + gen1.length(), str.length() - to.length() - creditor.getName().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                str.setSpan(new android.text.style.ForegroundColorSpan(ContextCompat.getColor(getContext(),R.color.colorAccent)), debtor.getName().length() + gen1.length(), str.length() - to.length() - creditor.getName().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                str.append(".");
                textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_debts_24px,0,0,0);
                textView.setCompoundDrawablePadding((int) (8 * Resources.getSystem().getDisplayMetrics().density));
                textView.setText(str);
            }
            textView.setPadding((int) (16 * Resources.getSystem().getDisplayMetrics().density)
                    ,(int) (8 * Resources.getSystem().getDisplayMetrics().density)
                    ,(int) (16 * Resources.getSystem().getDisplayMetrics().density)
                    ,(int) (8 * Resources.getSystem().getDisplayMetrics().density));
            debtLayout.addView(textView);
            noDebts = false;
        }
        if(noDebts){
            TextView textView = new TextView(getContext());
            final SpannableStringBuilder str = new SpannableStringBuilder(getString(R.string.generic_no_debts));
            //str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, str.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            str.setSpan(new android.text.style.ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.colorAccent)), 0, str.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            textView.setText(str);
            textView.setTextSize(17);
            textView.setPadding((int) (16 * Resources.getSystem().getDisplayMetrics().density)
                    , (int) (8 * Resources.getSystem().getDisplayMetrics().density)
                    , (int) (16 * Resources.getSystem().getDisplayMetrics().density)
                    , (int) (8 * Resources.getSystem().getDisplayMetrics().density));
            debtLayout.addView(textView);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mealDataManager.addDataChangeListener(listener);
    }

    @Override
    public void onPause() {
        super.onPause();
        mealDataManager.removeDataChangeListener(listener);
        mealExpenseData = null;
    }
}
