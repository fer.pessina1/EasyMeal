package com.pessinapps.easymeal.firebase;

import android.app.Activity;
import android.support.annotation.NonNull;

import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.pessinapps.easymeal.R;
import com.pessinapps.easymeal.util.DataChangeListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by fernando.pessina on 12/10/2017.
 *
 * Manage firebase authentication.
 *
 */

public class AuthManager {

    private static AuthManager instance;
    private FirebaseAuth mFirebaseAuth;
    private String mUsername;
    private String mUid;
    private static final String ANONYMOUS = "no user";
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    public static final int RC_SIGN_IN = 1;
    private static List<DataChangeListener> listeners = new ArrayList<>();

    private AuthManager(final Activity activity){
        mFirebaseAuth = FirebaseAuth.getInstance();
        mUsername = null;
        mUid = null;
        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if(user != null){
                    //user signed in
                    onSignedInInitialized(user.getDisplayName(), user.getUid());
                }else {
                    onSignedOutCleanup();
                    activity.startActivityForResult(
                            AuthUI.getInstance().createSignInIntentBuilder()
                                    .setIsSmartLockEnabled(false)
                                    .setProviders(Arrays.asList(new AuthUI.IdpConfig.Builder(AuthUI.EMAIL_PROVIDER).build(),
                                            new AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build()))
                                    .setTheme(R.style.AuthTheme)
                                    .build(), RC_SIGN_IN);
                }
            }
        };
    }
    public static synchronized AuthManager getInstance(Activity activity){
        if(instance == null){
            instance = new AuthManager(activity);
        }
        return instance;
    }
    public static synchronized AuthManager getInstance(){
        return instance;
    }

    private void onSignedOutCleanup() {
        mUsername = null;
        triggerListeners();
    }
    private void onSignedInInitialized(String username, String uid) {
        mUsername = username;
        mUid = uid;
        triggerListeners();
    }
    public void logout(){
        mFirebaseAuth.signOut();
    }
    public void attachListener(){
        mFirebaseAuth.addAuthStateListener(mAuthStateListener);
    }
    public void detachListener(){
        mFirebaseAuth.removeAuthStateListener(mAuthStateListener);
    }


    public void addUserListener(DataChangeListener l) {
        listeners.add(l);
        l.onDataChange();
    }
    public void removeUserListener(DataChangeListener listener){
        if(listeners.contains(listener)){
            listeners.remove(listener);
        }
    }
    private void triggerListeners(){
        for (DataChangeListener l : listeners) {
            l.onDataChange();
        }
    }

    public String getUsername(){
        return mUsername;
    }
    public String getUid(){
        return mUid;
    }
}
