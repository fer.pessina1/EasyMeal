package com.pessinapps.easymeal.firebase;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.pessinapps.easymeal.firebase.model.Expense;
import com.pessinapps.easymeal.firebase.model.MealExpenseData;
import com.pessinapps.easymeal.firebase.model.Payment;
import com.pessinapps.easymeal.firebase.model.Person;
import com.pessinapps.easymeal.util.DataChangeListener;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Created by fernando.pessina on 13/10/2017.
 *
 * Manages all data related to current meal
 */

public class MealDataManager {
    private static MealDataManager instance;

    private String key;
    private String uid;
    private MealExpenseData mealExpenseData;

    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mExpensesReference;
    private DatabaseReference mActiveExpenseReference;
    private DatabaseReference mUserExpenseKeyReference;
    private ValueEventListener ExpenseListener;
    private static List<DataChangeListener> listeners = new ArrayList<>();

    private MealDataManager() {
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mFirebaseDatabase.setPersistenceEnabled(false);

        AuthManager.getInstance().addUserListener(new DataChangeListener() {
            @Override
            public void onDataChange() {
                uid = AuthManager.getInstance().getUid();
                if(uid != null) {
                    mUserExpenseKeyReference = mFirebaseDatabase.getReference().child("users").child(uid).child("expense");
                    mExpensesReference = mFirebaseDatabase.getReference().child("expenses");
                    getExpenseKeyFromFirebase();
                }
            }
        });
    }
    public static synchronized MealDataManager getInstance(){
        if(instance == null){
            instance = new MealDataManager();
        }
        return instance;
    }

    private void getExpenseKeyFromFirebase() {
        ValueEventListener expenseKeyListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                key = dataSnapshot.getValue(String.class);
                if (key != null)
                    attachExpenseListener();
                else
                    createMeal();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };
        mUserExpenseKeyReference.addListenerForSingleValueEvent(expenseKeyListener);
    }
    private void attachExpenseListener() {
        if (ExpenseListener == null) {
            ExpenseListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mealExpenseData = dataSnapshot.getValue(MealExpenseData.class);
                    if(mealExpenseData!=null) {
                        if (!mealExpenseData.getUsers().contains(uid)) {
                            mealExpenseData.getUsers().add(uid);
                            mActiveExpenseReference.child("users").setValue(mealExpenseData.getUsers());
                        }
                    }
                    triggerListeners();
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            };
            if(key != null) {
                mActiveExpenseReference = mExpensesReference.child(key);
                mActiveExpenseReference.addValueEventListener(ExpenseListener);
            }
        } else {
            detachExpenseListener();
            attachExpenseListener();
        }
    }
    private void detachExpenseListener() {
        if (ExpenseListener != null) {
            mActiveExpenseReference.removeEventListener(ExpenseListener);
            ExpenseListener = null;
        }
    }

    public void createMeal(){
        mActiveExpenseReference = mExpensesReference.push();
        key = mActiveExpenseReference.getKey();
        mealExpenseData = new MealExpenseData(key);
        mealExpenseData.getUsers().add(uid);
        mActiveExpenseReference.setValue(mealExpenseData);
        mUserExpenseKeyReference.setValue(key);
        triggerListeners();
    }

    public void addDataChangeListener(DataChangeListener l) {
        listeners.add(l);
        l.onDataChange();
    }
    public void removeDataChangeListener(DataChangeListener listener) {
        if (listeners.contains(listener)) {
            listeners.remove(listener);
        }
    }
    private void triggerListeners() {
        for (DataChangeListener l : listeners) {
            l.onDataChange();
        }
    }
    public void leaveMeal(){
        mUserExpenseKeyReference.removeValue();
        detachExpenseListener();
        if(mealExpenseData != null) {
            mealExpenseData.getUsers().remove(uid);
            if (mealExpenseData.getUsers().size() == 0) {
                mActiveExpenseReference.removeValue();
            } else {
                mActiveExpenseReference.child("users").setValue(mealExpenseData.getUsers());
            }
            mealExpenseData = null;
        }
    }
    public void joinMeal(String key) {
        leaveMeal();
        mUserExpenseKeyReference.setValue(key);
        this.key = key;
        attachExpenseListener();
    }
    @Override
    protected void finalize() throws Throwable {
        key = null;
        detachExpenseListener();
        super.finalize();
    }


    public MealExpenseData getData() {
        return mealExpenseData;
    }
    public void addExpense(Expense expense){
        List<Person> beneficiaries = expense.getBeneficiaries();
        List<Person> members = mealExpenseData.getMembers();
        List<Person> found = new ArrayList<>();
        for(Person p : beneficiaries){
            if(!members.contains(p)){
                found.add(p);
            }
        }
        beneficiaries.removeAll(found);
        expense.setBeneficiaries(beneficiaries);
        if(members.contains(expense.getCreator())) {
            mealExpenseData.getExpenses().add(expense);
            mActiveExpenseReference.child("expenses").setValue(mealExpenseData.getExpenses());
        }
    }
    public void removeExpense(Expense expense){
        mealExpenseData.getExpenses().remove(expense);
        mActiveExpenseReference.child("expenses").setValue(mealExpenseData.getExpenses());
    }
    public void editExpense(Expense oldExpense, Expense newExpense){
        if(mealExpenseData.getExpenses().contains(oldExpense)) {
            mealExpenseData.getExpenses().remove(oldExpense);
            mealExpenseData.getExpenses().add(newExpense);
            mActiveExpenseReference.child("expenses").setValue(mealExpenseData.getExpenses());
        }
    }
    public void addPayment(Payment payment){
        mealExpenseData.getPayments().add(payment);
        mActiveExpenseReference.child("payments").setValue(mealExpenseData.getPayments());
    }
    public void removePayment(Payment payment){
        mealExpenseData.getPayments().remove(payment);
        mActiveExpenseReference.child("payments").setValue(mealExpenseData.getPayments());
    }
    public void addMember(Person member, Set<Expense> expensesList){
        mealExpenseData.getMembers().add(member);
        for (Expense expense : mealExpenseData.getExpenses()) {
            if(expensesList.contains(expense)){
                expense.getBeneficiaries().add(member);
            }
        }
        if(mActiveExpenseReference != null) {
            mActiveExpenseReference.setValue(mealExpenseData);
        }else{ // no active expense??
            createMeal();
        }
    }
    public void removeMember(Person member){
        Iterator<Expense> expenseIterator = mealExpenseData.getExpenses().iterator();
        while (expenseIterator.hasNext()) {
            Expense expense = expenseIterator.next();
            if (expense.getCreator().equals(member))
                expenseIterator.remove();
            else if (expense.getBeneficiaries().contains(member)) {
                expense.getBeneficiaries().remove(member);
            }
        }
        Iterator<Payment> paymentIterator = mealExpenseData.getPayments().iterator();
        while (paymentIterator.hasNext()) {
            Payment payment = paymentIterator.next();
            if (payment.getFrom().equals(member) || payment.getTo().equals(member))
                paymentIterator.remove();
        }
        mealExpenseData.getMembers().remove(member);
        mActiveExpenseReference.setValue(mealExpenseData);
    }
    public String getExpenseKey() {
        if (key == null) {
            createMeal();
        }
        return key;
    }
}
