package com.pessinapps.easymeal.firebase.model;

public class Debt {
    private Person creditor;
    private Person debtor;
    private Float amount;

    public Debt() {
    }

    public Debt(Person creditor, Person debtor, Float amount) {
        this.creditor = creditor;
        this.debtor = debtor;
        this.amount = amount;
    }

    public Person getCreditor() {
        return creditor;
    }

    public void setCreditor(Person creditor) {
        this.creditor = creditor;
    }

    public Person getDebtor() {
        return debtor;
    }

    public void setDebtor(Person debtor) {
        this.debtor = debtor;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!Debt.class.isAssignableFrom(obj.getClass())) {
            return false;
        }
        final Debt other = (Debt) obj;
        if ((this.creditor == null) ? (other.creditor != null) : !this.creditor.equals(other.creditor)) {
            return false;
        }
        if ((this.debtor == null) ? (other.debtor != null) : !this.debtor.equals(other.debtor)) {
            return false;
        }
        if ((this.amount == null) ? (other.amount != null) : !this.amount.equals(other.amount)) {
            return false;
        }
        return true;
    }
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + (this.debtor != null ? this.debtor.hashCode() : 0);
        hash = 53 * hash + (this.creditor != null ? this.creditor.hashCode() : 0);
        hash = 53 * hash + (this.amount != null ? this.amount.hashCode() : 0);
        return hash;
    }
}
