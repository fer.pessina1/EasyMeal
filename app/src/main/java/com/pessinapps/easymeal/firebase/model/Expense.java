package com.pessinapps.easymeal.firebase.model;

import java.util.ArrayList;
import java.util.List;

public class Expense {
    private float amount;
    private String concept;
    private Person creator;
    private List<Person> beneficiaries;

    public Expense() {
    }

    public Expense(float amount, String concept, Person creator, List<Person> beneficiaries) {
        this.amount = amount;
        this.concept = concept;
        this.creator = creator;
        this.beneficiaries = new ArrayList<>(beneficiaries);
    }

    public String getConcept() {
        return concept;
    }

    public void setConcept(String concept) {
        this.concept = concept;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public Person getCreator() {
        return creator;
    }

    public void setCreator(Person creator) {
        this.creator = creator;
    }

    public List<Person> getBeneficiaries() {
        return beneficiaries;
    }

    public void setBeneficiaries(List<Person> beneficiaries) {
        this.beneficiaries = beneficiaries;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!Expense.class.isAssignableFrom(obj.getClass())) {
            return false;
        }
        final Expense other = (Expense) obj;
        if ((this.creator == null) ? (other.creator != null) : !this.creator.equals(other.creator)) {
            return false;
        }
        if ((this.concept == null) ? (other.concept != null) : !this.concept.equals(other.concept)) {
            return false;
        }
        if ((this.beneficiaries == null) ? (other.beneficiaries != null) : !this.beneficiaries.equals(other.beneficiaries)) {
            return false;
        }
        if (this.amount != other.amount) {
            return false;
        }

        return true;
    }
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + (this.creator != null ? this.creator.hashCode() : 0);
        hash = 53 * hash + (this.concept != null ? this.concept.hashCode() : 0);
        hash = 53 * hash + (this.beneficiaries != null ? this.beneficiaries.hashCode() : 0);
        hash = 53 * hash + Float.valueOf(this.amount).hashCode();
        return hash;
    }
}
