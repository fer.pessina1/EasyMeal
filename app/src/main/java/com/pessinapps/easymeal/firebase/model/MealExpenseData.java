package com.pessinapps.easymeal.firebase.model;

import com.google.firebase.database.Exclude;

import java.util.ArrayList;
import java.util.List;

public class MealExpenseData {
    private String mealName;
    private List<Expense> expenses;
    private List<Person> members;
    private List<Payment> payments;
    private List<String> users;

    public MealExpenseData() {
    }

    public MealExpenseData(String mealName) {
        this.mealName = mealName;
        expenses = new ArrayList<>();
        members = new ArrayList<>();
        payments = new ArrayList<>();
        users = new ArrayList<>();
    }

    public MealExpenseData(String mealName, List<Expense> expenses, List<Person> members, List<Payment> payments) {
        this.mealName = mealName;
        this.expenses = expenses;
        this.members = members;
        this.payments = payments;
    }

    public String getMealName() {
        if(mealName == null)
            mealName = "";
        return mealName;
    }

    public void setMealName(String mealName) {
        this.mealName = mealName;
    }

    public List<Expense> getExpenses() {
        if(expenses == null)
            expenses = new ArrayList<>();
        return expenses;
    }

    public void setExpenses(List<Expense> expenses) {
        this.expenses = expenses;
    }

    public List<Person> getMembers() {
        if(members == null)
            members = new ArrayList<>();
        return members;
    }

    public void setMembers(List<Person> members) {
        this.members = members;
    }

    public List<Payment> getPayments() {
        if(payments == null){
            payments = new ArrayList<>();
        }
        return payments;
    }

    public void setPayments(List<Payment> payments) {
        this.payments = payments;
    }

    @Exclude
    public List<String> getMemberNames(){
        List<String> memberNames = new ArrayList<>();
        if(members != null) {
            for (Person member : members) {
                memberNames.add(member.getName());
            }
        }
        return memberNames;
    }

    public float getMemberShare(Person person){
        float ret = 0;
        if(expenses != null) {
            for (Expense expense : expenses) {
                if (expense.getBeneficiaries().contains(person)) {
                    ret += expense.getAmount() / expense.getBeneficiaries().size();
                }
            }
        }
        return ret;
    }

    public float getMemberExpenseTotal(Person person){
        float ret = 0;
        if(expenses != null) {
            for (Expense expense : expenses) {
                if (expense.getCreator().equals(person)) {
                    ret += expense.getAmount();
                }
            }
        }
        return ret;
    }

    public List<String> getUsers() {
        if(users == null){
            users = new ArrayList<>();
        }
        return users;
    }

    public void setUsers(List<String> users) {
        this.users = users;
    }


}
