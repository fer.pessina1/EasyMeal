package com.pessinapps.easymeal.firebase.model;

public class Payment {
    private Person from;
    private Person to;
    private float amount;

    public Payment() {
    }

    public Payment(Person from, Person to, float amount) {
        this.from = from;
        this.to = to;
        this.amount = amount;
    }

    public Person getFrom() {
        return from;
    }

    public void setFrom(Person from) {
        this.from = from;
    }

    public Person getTo() {
        return to;
    }

    public void setTo(Person to) {
        this.to = to;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!Payment.class.isAssignableFrom(obj.getClass())) {
            return false;
        }
        final Payment other = (Payment) obj;
        if ((this.from == null) ? (other.from != null) : !this.from.equals(other.from)) {
            return false;
        }
        if ((this.to == null) ? (other.to != null) : !this.to.equals(other.to)) {
            return false;
        }
        if (this.amount != other.amount) {
            return false;
        }
        return true;
    }
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + (this.from != null ? this.from.hashCode() : 0);
        hash = 53 * hash + (this.to != null ? this.to.hashCode() : 0);
        hash = 53 * hash + Float.valueOf(this.amount).hashCode();
        return hash;
    }
}
