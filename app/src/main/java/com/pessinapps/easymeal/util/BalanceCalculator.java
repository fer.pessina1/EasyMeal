package com.pessinapps.easymeal.util;


import com.pessinapps.easymeal.firebase.model.Debt;
import com.pessinapps.easymeal.firebase.model.Expense;
import com.pessinapps.easymeal.firebase.model.MealExpenseData;
import com.pessinapps.easymeal.firebase.model.Payment;
import com.pessinapps.easymeal.firebase.model.Person;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class BalanceCalculator {

    public static Map<Person, Float> getAllBalances(MealExpenseData data){
        Map<Person,Float> balances = new ConcurrentHashMap<>();
        for (Person person : data.getMembers()) {
            Float balance = 0F;
            for (Expense expense : data.getExpenses()) {
                if(expense.getBeneficiaries().contains(person)) {
                    Float share = expense.getAmount() / expense.getBeneficiaries().size();
                    if(expense.getCreator().equals(person)){
                        balance += expense.getAmount()-share;
                    }else {
                        balance-=share;
                    }
                }
            }
            for (Payment payment : data.getPayments()) {
                if(payment.getTo().equals(person)){
                    balance -= payment.getAmount();
                }else if(payment.getFrom().equals(person)){
                    balance += payment.getAmount();
                }
            }
            balances.put(person,balance);
        }
        return balances;
    }

    public static List<Debt> filterDebts(List<Debt> all, Person person){
        List<Debt> filtered = new ArrayList<>();
        for (Debt debt : all) {
            if(debt.getDebtor().equals(person)){
                filtered.add(debt);
            }
        }
        return filtered;
    }

    public static List<Debt> calculateDebts(Map<Person, Float> b){
        List<Debt> debts = new ArrayList<>();

        Map<Person,Map<Person,Float>> allDebts = new ConcurrentHashMap<>();
        for (Map.Entry<Person, Float> entry : b.entrySet()) {
            allDebts.put(entry.getKey(),new ConcurrentHashMap<Person, Float>());
        }
        Map<Person, Float> balances = new ConcurrentHashMap<>(b);
        Iterator<Map.Entry<Person, Float>> iterator = balances.entrySet().iterator();
        while(iterator.hasNext()){
            Map.Entry<Person, Float> balanceEntry = iterator.next();
            Float balance1 = balanceEntry.getValue();
            Iterator<Map.Entry<Person, Float>> iterator2 = balances.entrySet().iterator();
            while(balance1<0 && iterator2.hasNext()){
                Map.Entry<Person, Float> balanceEntry2 = iterator2.next();
                Float balance2 = balanceEntry2.getValue();
                if(balance2>0){ // balanceEntry is negative and balanceEntry2 is positive, generate debt between these two people
                    if((0-balance1) > balance2){
                        balance1 += balance2;
                        allDebts.get(balanceEntry.getKey()).put(balanceEntry2.getKey(),balance2);
                        balance2 = 0F;
                        balances.put(balanceEntry2.getKey(),balance2);
                    }else if((0-balance1) < balance2){
                        allDebts.get(balanceEntry.getKey()).put(balanceEntry2.getKey(),-balance1);
                        balance2 += balance1;
                        balances.put(balanceEntry2.getKey(),balance2);
                        balance1 = 0F;
                    }else{
                        allDebts.get(balanceEntry.getKey()).put(balanceEntry2.getKey(),balance2);
                        balance1 = 0F;
                        balance2 = 0F;
                        balances.put(balanceEntry2.getKey(),balance2);
                    }
                }
            }
            balances.put(balanceEntry.getKey(),balance1);
        }

        for (Map.Entry<Person, Map<Person, Float>> entry : allDebts.entrySet()) {
            Person debtor = entry.getKey();
            for (Map.Entry<Person, Float> debtEntry : entry.getValue().entrySet()) {
                debts.add(new Debt(debtEntry.getKey(),debtor,debtEntry.getValue()));
            }
        }
        return debts;
    }

}
