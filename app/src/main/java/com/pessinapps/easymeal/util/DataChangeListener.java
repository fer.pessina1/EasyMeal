package com.pessinapps.easymeal.util;

public interface DataChangeListener {
    public void onDataChange();
}
