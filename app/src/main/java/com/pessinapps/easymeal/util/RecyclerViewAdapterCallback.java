package com.pessinapps.easymeal.util;

import com.pessinapps.easymeal.firebase.model.Expense;

/**
 * Created by fernando.pessina on 18/10/2017.
 *
 * Used to communicate between adapter and fragment
 */

public interface RecyclerViewAdapterCallback {
    void onMethodCallback(Object object, int action);
}
